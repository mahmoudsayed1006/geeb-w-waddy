import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";

const OrderSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    country:{
        type:Number,
        ref:"country",
        default: 1
    },
    client: {
        type: Number,
        ref: 'user',
        required:true
    },
    salesMan: {
        type: Number,
        ref: 'user',
    },
    offer: {
        type: Number,
        ref: 'offer',
    },
    bill: {
        type: Number,
        ref: 'bill',
    },
    shopName: {
        type: String,
    },
    modal: {
        type: String,
    },
    description: {
        type: String,
        required:true,
    },
    img: {
        type: String,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    total: {
        type: Number,
    },
    clientDestination: {
        type: [Number] 
    },
    shopDestination: {
        type: [Number] 
    },
    time:{
        type:String,
        required:true
    },
    status: {
        type: String,
        enum: ['PENDING','CANCEL','ON_PROGRESS','RECEIVED','ON_THE_WAY','ARRIVED', 'DELIVERED'],
        default: 'PENDING'
    },
    rate:{
        type: Number,
    },
    rateText:{
        type: String,
    },
    reason:{
        type: String,
    },
    paidFromBalance:{
        type:Boolean,
        default:false
    },
    accept:{
        type:Boolean,
        default:false
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

OrderSchema.index({ location: '2dsphere' });
OrderSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        if (ret.destination) {
            ret.destination = ret.destination.coordinates;
        }
      
    }
});
autoIncrement.initialize(mongoose.connection);
OrderSchema.plugin(autoIncrement.plugin, { model: 'order', startAt: 1 });

export default mongoose.model('order', OrderSchema);