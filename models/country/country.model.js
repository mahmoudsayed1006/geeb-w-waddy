import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const countrySchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    countryName: {
        type: String,
        required: true,
        trim: true,
    },
    arabicName: {
        type: String,
        required: true,
        trim: true,
    },
    img:{
        type: String,
        
    },
    currency:{
        type: String,
        required: true,
    },
    arabicCurrency:{
        type: String,
        required: true,
    },
    giftBalance:{
        type: Number,
        default:0
    },
    disLimit:{
        type: Number,
        default:0
    },
    disRatio:{
        type: Number,
        default:0
    },
    giftType:{
        type:String,
        enum: ['RATIO', 'NUMBER'],
        default:'NUMBER'
    },
    enableGift:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

countrySchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



countrySchema.plugin(autoIncrement.plugin, { model: 'country', startAt: 1 });

export default mongoose.model('country', countrySchema);
