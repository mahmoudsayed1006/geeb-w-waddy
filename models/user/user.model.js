import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
import { isImgUrl } from "../../helpers/CheckMethods";

const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    username: {
        type: String,
        //required: true,
    },
    email: {
        type: String,
        trim: true,
        required: true,
        validate: {
            validator: (email) => isEmail(email),
            message: 'Invalid Email Syntax'
        }

    },
    phone: {
        type: String,
       // required: true,
        trim: true,
    },
    gender: {
        type: String,
        //required: true,
        trim: true,
    },
    birthYear: {
        type: String,
        //required: true,
        trim: true,
    },
    type: {
        type: String,
        enum: ['CLIENT', 'ADMIN','SALES-MAN'],
        required:true
    },
    language:{
        type:String,
    },
    active:{
        type: Boolean,
        default: false
    },
    block:{
        type: Boolean,
        default: false
    },
    img: {
        type: String,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    transportType:{
        type:String
    },
    manufacturingYear:{
        type:String
    },
    bank:{
        type:String
    },
    bankNumber:{
        type:String
    },
    transportImages: {
        type: String,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    transportLicense: {
        type: String,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    verifycode: {
        type: Number
    },
    token:{
        type:[String],
    },
    signUpFrom:{
        type:String
    },
    tasksCount:{
        type:Number,
        default:0
    },
    rate:{
        type:Number,
        default:0
    },
    ratePercent:{
        type:Number,
        default:0
    },
    balance:{
        type:Number,
        default:0
    },
    Giftbalance:{
        type:Number,
        default:0
    },
    addBalance:{
        type:Number,
        default:0
    },
    debt:{
        type:Number,
        default:0
    },
    hasCoupon:{
        type: Boolean,
        default: false
    },
    coupon:{
        type:Number,
        ref:"coupon"
    },
    country:{
        type:Number,
        ref:"country",
        default: 1
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });


userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
export default mongoose.model('user', userSchema);