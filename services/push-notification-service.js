import * as admin from 'firebase-admin';
import User from '../models/user/user.model';

const serviceAccount = require('../service-account.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://geebwewady.firebaseio.com"
  });

export async function sendPushNotification(notifi, title) {

    let user = await User.findById(notifi.targetUser);
    console.log(user);
    let tokens = user.token;
    console.log(tokens);
    const payload = {
        notification: {
            title: notifi.text,
            sound: 'default',
            icon: notifi.subject.toString(),
            body: notifi.subjectType
        },
    }

    console.log(payload);

    if (tokens && tokens.length >= 1) {
        console.log('TOKENS : ', tokens);

        admin.messaging().sendToDevice(tokens, payload)
            .then(response => {
                console.log('Successfully sent a message')//, response);
            })
            .catch(error => {
                console.log('Error sending a message:', error);
            });
    }

}