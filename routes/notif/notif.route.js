import express from 'express';
import NotifController from '../../controllers/notif/notif.controller';
const router = express.Router();

router.route('/')
    .get(NotifController.find);

router.route('/:notifId/read')
    .put(NotifController.read)

router.route('/:notifId/unread')
    .put(NotifController.unread)

router.route('/unreadCount')
    .get(NotifController.unreadCount);

router.route('/:notifId/delete')
    .delete(NotifController.delete);
export default router;