import express from 'express';
import CountryController from '../../controllers/country/country.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        multerSaveTo('country').single('img'),
        CountryController.validateCountryBody(),
        CountryController.create
    )
    .get(CountryController.getAllPaginated);

router.route('/:countryId')
    .put(
        requireAuth,
        multerSaveTo('country').single('img'),
        CountryController.validateCountryBody(true),
        CountryController.update
    )
    .get(CountryController.getById)
    .delete(requireAuth,CountryController.delete);

router.route('/:countryId/addGift')
    .put(
        requireAuth,
        CountryController.validateAddGift(),
        CountryController.addGift
    )
router.route('/:countryId/removeGift')
    .put(
        requireAuth,
        CountryController.removeGift
    )


export default router;