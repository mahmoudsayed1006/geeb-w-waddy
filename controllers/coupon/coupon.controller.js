import ApiResponse from "../../helpers/ApiResponse";
import Coupon from "../../models/coupon/coupon.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';

import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { country } = req.query;
            let query = {deleted: false };
            if (country){
                query.country = country;
            }
            let Coupons = await Coupon.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const CouponsCount = await Coupon.count(query);
            const pageCount = Math.ceil(CouponsCount / limit);

            res.send(new ApiResponse(Coupons, page, pageCount, limit, CouponsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('couponNumber').not().isEmpty().withMessage('number is required'),
            body('discount').not().isEmpty().withMessage('discount is required'),
            body('country').not().isEmpty().withMessage('country is required')
            .isNumeric().withMessage('numeric value required'),
        ];
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
            let createdCoupon = await Coupon.create({ ...validatedBody});
            let reports = {
                "action":"Create Coupon",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdCoupon);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { CouponId } = req.params;
            await checkExist(CouponId, Coupon, { deleted: false });
            let Coupon = await Coupon.findById(CouponId);
            res.send(Coupon);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { CouponId } = req.params;
            await checkExist(CouponId, Coupon, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedCoupon = await Coupon.findByIdAndUpdate(CouponId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Coupon",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedCoupon);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { CouponId } = req.params;
            let coupon = await checkExistThenGet(CouponId, Coupon, { deleted: false });
            coupon.deleted = true;
            await coupon.save();
            let reports = {
                "action":"Delete Coupon",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    
    async end(req, res, next) {
        try {
            let user = req.user;
           
            let { couponId } = req.params;
            let coupon = await checkExistThenGet(couponId, Coupon);
            coupon.end = true;
            await coupon.save();
            let reports = {
                "action":"End Coupon",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send(coupon);

        }
        catch (err) {
            next(err);
        }
    },
    async reused(req, res, next) {
        try {
            let user = req.user;
            let { couponId } = req.params;
            let coupon = await checkExistThenGet(couponId, Coupon);
            coupon.end = false;
            await coupon.save();
            let reports = {
                "action":"reuse Coupon",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send(coupon);

        }
        catch (err) {
            next(err);
        }
    },
};