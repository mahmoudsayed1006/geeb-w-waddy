import ApiResponse from "../../helpers/ApiResponse";
import Category from "../../models/category/category.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';

import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
const populateQuery = [
    { path: 'country', model: 'country' },
   
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { type,services,country } = req.query;
            let query = {deleted: false };
            if (country){
                query.country = country;
            }
            if (type){
                query.type = type;
            }
            if (services){
                query.services = services;
            }
            
            let categories = await Category.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const categoriesCount = await Category.count(query);
            const pageCount = Math.ceil(categoriesCount / limit);

            res.send(new ApiResponse(categories, page, pageCount, limit, categoriesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let categories = await Category.find(query)
                .sort({ createdAt: -1 });
            res.send(categories)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('categoryname').not().isEmpty().withMessage('categoryname is required')
                .custom(async (val, { req }) => {
                    let query = { categoryname: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.categoryId };

                    let category = await Category.findOne(query).lean();
                    if (category)
                        throw new Error('category duplicated name');

                    return true;
                }),
            body('arabicname').not().isEmpty().withMessage('arabicname is required')
                .custom(async (val, { req }) => {
                    let query = { arabicname: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.categoryId };

                    let category = await Category.findOne(query).lean();
                    if (category)
                        throw new Error('category duplicated name');

                    return true;
                }),
            body('type').not().isEmpty().withMessage('type is required'),
            body('description'),
            body('arabicType'),
            body('arabicDescription'),
            body('services'),
            body('modal'), //اقل موديل
            body('country').not().isEmpty().withMessage('country is required')
                .isNumeric().withMessage('numeric value required'),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
            if(validatedBody.services){
                validatedBody.services = true
            } else{
                validatedBody.services = false
            }
            let image = await handleImg(req);
            console.log(image);
             console.log(image);
            let createdCategory = await Category.create({ ...validatedBody,img:image});

            let reports = {
                "action":"Create Category",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdCategory);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { categoryId } = req.params;
            await checkExist(categoryId, Category, { deleted: false });
            let category = await Category.findById(categoryId);
            res.send(category);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { categoryId } = req.params;
            await checkExist(categoryId, Category, { deleted: false });

            const validatedBody = checkValidations(req);
            if(validatedBody.services){
                validatedBody.services = true
            } else{
                validatedBody.services = false
            }
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let updatedCategory = await Category.findByIdAndUpdate(categoryId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Category",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedCategory);
        }
        catch (err) {
            next(err);
        }
    },
    async service(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { categoryId } = req.params;
            let category = await checkExistThenGet(categoryId, Category, { deleted: false });
            category.services = true;
            await category.save();
            let reports = {
                "action":" Category became sarvices",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send(category);

        }
        catch (err) {
            next(err);
        }
    },
    async unservice(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { categoryId } = req.params;
            let category = await checkExistThenGet(categoryId, Category, { deleted: false });
            category.services = false;
            await category.save();
            res.status(204).send(category);

        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { categoryId } = req.params;
            let category = await checkExistThenGet(categoryId, Category, { deleted: false });
            category.deleted = true;
            await category.save();
            let reports = {
                "action":"Delete Category",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};