import Country from "../../models/country/country.model";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExistThenGet } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import Category from "../../models/category/category.model";

export default {
    validateCountryBody(isUpdate = false) {
        let validations = [
            body('countryName').not().isEmpty().withMessage('countryName Required')
                .custom(async (value, { req }) => {
                    let userQuery = { countryName: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.countryId };
                    if (await Country.findOne(userQuery))
                        throw new Error('countryName duplicated');
                    else
                        return true;
                }),
                body('arabicName').not().isEmpty().withMessage('country arabic name Required')
                .custom(async (value, { req }) => {
                    let userQuery = { arabicName: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.countryId };
                    if (await Country.findOne(userQuery))
                        throw new Error('arabic country name duplicated');
                    else
                        return true;
                }),
                body('currency').not().isEmpty().withMessage('currency Required'),
                body('arabicCurrency').not().isEmpty().withMessage('arabic currency Required'),
                body('giftBalance').optional(),
                body('giftType').optional()
                .isIn(['RATIO', 'NUMBER']).withMessage('wrong type'),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);
        return validations;
    },
    async create(req, res, next) {
        try {
            let user = req.user;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));

            const validatedBody = checkValidations(req);
            let image = await handleImg(req);
            let country = await Country.create({ ...validatedBody,img:image });
            return res.status(201).send(country);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let countries = await Country.find({ deleted: false })
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });

            let count = await Country.count({ deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(countries, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        } 
    }, 

    async update(req, res, next) {
        try {
            let user = req.user;
            let { countryId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let country  = await Country.findByIdAndUpdate(countryId, {
                ...validatedBody,
            }, { new: true });
            
            return res.status(200).send(country);
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { countryId } = req.params;

            let country = await checkExistThenGet(countryId, Country, { deleted: false });
            return res.send(country);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { countryId } = req.params;

        try {
            let country = await checkExistThenGet(countryId, Country, { deleted: false });
            let categories = await Category.find({ country: countryId });
            for (let categoryId of categories) {
                categoryId.deleted = true;
                await categoryId.save();
            }
            country.deleted = true;
            await country.save();
            res.send('deleted');

        } catch (err) {
            next(err);
        }
    },
    validateAddGift() {
        return [
            body('giftBalance').not().isEmpty().withMessage('gift Balance is required')
            .isNumeric().withMessage('numeric value required'),
            body('giftType').not().isEmpty().withMessage('gift Type is required')
            .isIn(['RATIO', 'NUMBER']).withMessage('wrong type'),
            body('disLimit').optional(),
            body('disRatio').optional(),
        ];
       
    },
    async addGift(req, res, next) {
        let { countryId } = req.params;
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 
            const validatedBody = checkValidations(req);
            let country = await checkExistThenGet(countryId, Country, { deleted: false });
            country.enableGift = true;
            country.giftBalance = validatedBody.giftBalance;
            country.giftType = validatedBody.giftType;
            if(validatedBody.disLimit){
                country.disLimit = validatedBody.disLimit;
            }
            if(validatedBody.disRatio){
                country.disRatio = validatedBody.disRatio;
            }
            
            await country.save();
            res.send({country});

        } catch (err) {
            next(err);
        }
    },
    async removeGift(req, res, next) {
        let { countryId } = req.params;
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 
            let country = await checkExistThenGet(countryId, Country, { deleted: false });
            country.enableGift = false;
            await country.save();
            res.send({country});

        } catch (err) {
            next(err);
        }
    }
}