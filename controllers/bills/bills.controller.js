import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Bill from "../../models/bills/bill.model";
import Offer from "../../models/offer/offer.model";
import Order from "../../models/order/order.model";
import Notif from "../../models/notif/notif.model";
import User from "../../models/user/user.model";
import Coupon from "../../models/coupon/coupon.model";
import Tax from "../../models/tax/tax.model";
import Country from "../../models/country/country.model";

import { sendNotifiAndPushNotifi } from "../../services/notification-service";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {deleted: false };
            let bills = await Bill.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const billCount = await Bill.count(query);
            const pageCount = Math.ceil(billCount / limit);

            res.send(new ApiResponse(bills, page, pageCount, limit, billCount, req));
        } catch (err) {
            next(err);
        }
    },
    
    validateBody(isUpdate = false) {
        let validations = [
            body('cost').not().isEmpty().withMessage('description is required'),
        ];
        if (isUpdate)
        validations.push([
            body('img').not().isEmpty().withMessage('img is required').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let {orderId,offerId} = req.params;
            if (req.user.type != 'SALES-MAN')
                return next(new ApiError(403, ('sales man auth')));
            let order = await checkExistThenGet(orderId, Order);
            if(order.bill)
                return next(new ApiError(403, ('this order has bill')));
            let offer = await checkExistThenGet(offerId, Offer);
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(order.client, User);
            //الضريبه 
            let tax = await Tax.find({ deleted: false }).select("value");
            console.log(tax)
            let taxValue = tax[0].value;
            console.log(taxValue);
            let taxValueNumber = offer.deliveryCost * taxValue
            let taxFromTotal = taxValueNumber / 100;
            let vatNumber =  offer.deliveryCost * 5 ;
            let vat = vatNumber / 100
            //المبلغ الأصلي × نسبة الخصم ÷ 100
            
            let delivary;
           
            if(user.hasCoupon == true){
                let coupon = await checkExistThenGet(user.coupon, Coupon);
                let deliveryDiscount = (offer.deliveryCost * coupon.discount) / 100;
                delivary = (offer.deliveryCost - deliveryDiscount) + vat;
                validatedBody.delivaryCost = delivary;
               
            } else{
                delivary = offer.deliveryCost + vat;
                validatedBody.delivaryCost = delivary;
            }
            /*country */
            let country = await checkExistThenGet(order.country, Country);
            console.log(country)
            if(country.enableGift == true){
                
                if(country.giftType =='NUMBER'){
                    if(user.Giftbalance > 0 && user.Giftbalance > country.disLimit ){
                        delivary = delivary - country.disLimit;
                        user.Giftbalance = user.Giftbalance - country.disLimit
                        validatedBody.giftCount = country.disLimit;
                        validatedBody.gift = true;
                    }
                    if(user.Giftbalance > 0 && user.Giftbalance < country.disLimit ){
                        delivary = delivary - user.Giftbalance;
                        user.Giftbalance = 0;
                        validatedBody.giftCount = user.Giftbalance;
                        validatedBody.gift = true;
                    }
                }
                if(country.giftType =='RATIO'){
                    let disNumber =  delivary * country.disRatio ;
                    let dis = disNumber / 100;
                    //المبلغ الأصلي × نسبة الخصم ÷ 100
                    if(user.Giftbalance > 0 && user.Giftbalance > dis ){
                        delivary = delivary - dis;
                        user.Giftbalance = user.Giftbalance - dis
                        validatedBody.giftCount = dis;
                        validatedBody.gift = true;
                    }
                    if(user.Giftbalance > 0 && user.Giftbalance < dis ){
                        delivary = delivary - user.Giftbalance;
                        user.Giftbalance = 0;
                        validatedBody.giftCount = user.Giftbalance;
                        validatedBody.gift = true;
                    }
                
                }
            }
            
            let total = parseInt(validatedBody.cost) + delivary
            console.log(total)
            validatedBody.totalCost = total; //money in bill
            validatedBody.paidCost = Math.ceil(total);//money to paid it 
            console.log(validatedBody.totalCost)
            if (req.file) {
                let image = await handleImg(req)
                validatedBody.img = image;
             }
            let createdbill = await Bill.create({ ...validatedBody});
            order.bill = createdbill.id;
            await order.save();

           
            let addBalance = validatedBody.paidCost - total;
            console.log(addBalance.toPrecision(2))
            let newBalance = user.balance + addBalance.toPrecision(2);
            user.balance = parseFloat(newBalance);
            user.addBalance = addBalance.toPrecision(2);
            await user.save();
            sendNotifiAndPushNotifi({
                targetUser: order.client, 
                fromUser: req.user, 
                text: 'new notification',
                subject: createdbill.id,
                subjectType: req.user.username + ' send order bill'
            });
            let notif = {
                "description":req.user.username + ' send order bill',
                "arabicDescription":req.user.username + ' ارسل لك الفاتوره الخاصه بطلبك'
            }
            await Notif.create({...notif,resource:req.user,target:order.client,bill:createdbill.id});
            
            let salesMan = await checkExistThenGet(req.user._id, User);
            salesMan.debt = salesMan.debt + taxFromTotal;
            salesMan.balance = salesMan.balance + delivary;
            await salesMan.save();
            let reports = {
                "action":"Create bill",
            };
            let report = await Report.create({...reports, user: req.user});
            res.status(201).send(createdbill);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { billId } = req.params;
            await checkExist(billId, Bill, { deleted: false });
            let bill = await Bill.findById(billId);
            res.send(bill);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'SALES_MAN')
                return next(new ApiError(403, ('sales man auth')));

            let { billId } = req.params;
            await checkExist(billId, Bill, { deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let updatedbill = await Bill.findByIdAndUpdate(billId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update bill",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedbill);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            let { billId } = req.params;
            let bill = await checkExistThenGet(billId, Bill, { deleted: false });
            
            bill.deleted = true;
            await bill.save();
            let reports = {
                "action":"Delete bill",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};