import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Order from "../../models/order/order.model";
import Category from "../../models/category/category.model";
import Coupon from "../../models/coupon/coupon.model";
import Shop from "../../models/shop/shop.model";
import Report from "../../models/reports/report.model";
import Bill from "../../models/bills/bill.model";

const populateQuery = [
    { path: 'client', model: 'user' },
    { path: 'salesMan', model: 'user' },
];
const action = [
    { path: 'user', model: 'user' },
]
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
                $and: [
                    {deleted: false},
                    {type:'CLIENT'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

    async getLastOrder(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let { status} = req.query
            let query = {deleted: false };
            if (status)
                query.status = status;                
            let lastOrder = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastOrder);
        } catch (error) {
            next(error);
        }
    },
   
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const usersCount = await User.count({deleted:false,type:'CLIENT'});
            const salesMenCount = await User.count({deleted:false,type:'SALES-MAN'});
            const categoryCount = await Category.count(query);
            const shopCount = await Shop.count(query);
            const couponCount = await Coupon.count(query);
            const PendingOrdersCount = await Order.count({deleted:false,status:'PENDING'});
            const AcceptedOrdersCount = await Order.count({deleted:false,status:'ON_PROGRESS'});
            const RefusedOrdersCount = await Order.count({deleted:false,status:'CANCEL'});
            const DeliveredOrdersCount = await Order.count({deleted:false,status:'DELIVERED'});
            const OnWayOrdersCount = await Order.count({deleted:false,status:'ON_THE_WAY'});

            let total = await Bill.find(query).select('totalCost');
            var sum = 0;
            for (var i = 0; i < total.length; i++) { 
              sum += total[i].totalCost
            }
            
            res.status(200).send({
                clients:usersCount,
                salesMen:salesMenCount,
                categories:categoryCount,
                coupons:couponCount,
                shops:shopCount,
                pending:PendingOrdersCount,
                accepted:AcceptedOrdersCount,
                refused:RefusedOrdersCount,
                delivered:DeliveredOrdersCount,
                onWay:OnWayOrdersCount,
                totalSales:sum
                
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}