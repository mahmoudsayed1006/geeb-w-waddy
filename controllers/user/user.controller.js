import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { sendForgetPassword } from '../../services/message-service';
import { sendEmail } from "../../services/emailMessage.service";
import { toImgUrl } from "../../utils";
import { generateVerifyCode } from '../../services/generator-code-service';
import Notif from "../../models/notif/notif.model";
import Coupon from "../../models/coupon/coupon.model";
import ApiResponse from "../../helpers/ApiResponse";
import Country from "../../models/country/country.model";

import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}
const checkUserExistByEmail = async (email) => {
    let user = await User.findOne({ email });
    if (!user)
        throw new ApiError.BadRequest('email Not Found');

    return user;
}
const populateQuery = [
    { path: 'country', model: 'country' },
   
];
export default {
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            let phone = req.body.phone;
            let user = await User.findOne({ phone }).populate(populateQuery); 
            if(!user)
                return next(new ApiError(403, ('invalid phone')));
                if(req.body.token != null){
                    let arr = user.token; 
                    var found = arr.find(function(element) {
                        return element == req.body.token;
                    });
                    if(!found){
                        user.token.push(req.body.token);
                        await user.save();
                    }
                }
            
                res.status(200).send({
                    user,
                    token: generateToken(user.id)
                });
            
            let reports = {
                "action":"User Login",
            };

            let report = await Report.create({...reports, user: user });
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('username').not().isEmpty().withMessage('username is required'),
            body('gender').not().isEmpty().withMessage('gender is required'),
            body('birthYear').not().isEmpty().withMessage('birthYear is required'),
            body('language'),
            body('signUpFrom'),
            body('country'),

            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
               
                body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT','ADMIN','SALES-MAN']).withMessage('wrong type'),
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img'),

        ];
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            if(validatedBody.country){
                let country = await checkExistThenGet(validatedBody.country, Country);
                if(country.enableGift == true){
                    validatedBody.Giftbalance = country.giftBalance;
                }
            }
            
            
            if (req.file) {
               let image = await handleImg(req)
               validatedBody.img = image;
            }
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });
            res.status(201).send({
                user: await User.findOne(createdUser).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    validateSalesManCreateBody(isUpdate = false) {
        let validations = [
            body('username').not().isEmpty().withMessage('username is required'),
            body('gender').not().isEmpty().withMessage('gender is required'),
            body('birthYear').not().isEmpty().withMessage('birthYear is required'),
            body('transportType'),
            body('manufacturingYear'),
            body('bank'),
            body('bankNumber'),
            body('language'),
            body('signUpFrom'),

            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

                body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT','ADMIN','SALES-MAN']).withMessage('wrong type'),
                body('country').not().isEmpty().withMessage('country is required')
                .isNumeric().withMessage('numeric value required'),

        ];
        return validations;
    },
    async addSalesMan(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let country = await checkExistThenGet(validatedBody.country, Country);
            if(country.enableGift == true){
                validatedBody.Giftbalance = country.giftBalance;
            }
            validatedBody.active = true;
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.img = imagesList;
                }
                if (req.files['transportImages']) {
                    let imagesList = [];
                    for (let imges of req.files['transportImages']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.transportImages = imagesList;
                }
                if (req.files['transportLicense']) {
                    let imagesList = [];
                    for (let imges of req.files['transportLicense']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.transportLicense = imagesList;
                }
            }
            else {
                next(new ApiError(422, 'imgs is required'));
            }
            let createdUser = await User.create({
                ...validatedBody
            });
            res.status(201).send({
                user: await User.findOne(createdUser),
                token: generateToken(createdUser.id)
            });
            let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },

    async becameSalesMan(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            if (req.files) {
                if (req.files['transportImages']) {
                    let imagesList = [];
                    for (let imges of req.files['transportImages']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.transportImages = imagesList;
                }
                if (req.files['transportLicense']) {
                    let imagesList = [];
                    for (let imges of req.files['transportLicense']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.transportLicense = imagesList;
                }
            }
            else {
                next(new ApiError(422, 'imgs is required'));
            }
            let user = await checkExistThenGet(req.user._id, User); 
            if(req.body.transportType){
                user.transportType = req.body.transportType;
            } else{
                next(new ApiError(422, 'transportType is required'));

            }
            if(req.body.manufacturingYear){
                user.manufacturingYear = req.body.manufacturingYear;
            } else{
                next(new ApiError(422, 'manufacturingYear is required'));

            }
            if(req.body.bank){
                user.bank = req.body.bank;
            } else{
                next(new ApiError(422, 'bank is required'));

            }
            if(req.body.bankNumber){
                user.bankNumber = req.body.bankNumber;
            } else{
                next(new ApiError(422, 'bankNumber is required'));
            }
            
            if(validatedBody.transportImages){
                user.transportImages = validatedBody.transportImages;
            } else{
                next(new ApiError(422, 'transportImages is required'));
            }
            if(validatedBody.transportLicense){
                user.transportLicense = validatedBody.transportLicense;
            } else{
                next(new ApiError(422, 'transportLicense is required'));
            }
            await user.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: user.id,
                    subjectType: req.user.username + ' want to be sales man'
                });
                let notif = {
                    "description":req.user.username + ' want to be sales man',
                    "arabicDescription":req.user.username + " يرغب فى الانضمام الى مندوبينا"
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,user:user.id});
            });
            
            
            console.log(validatedBody)

            let reports = {
                "action":"User Wants to be Sales Man",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(user);

        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.type = 'SALES-MAN';
            activeUser.active = true;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: userId, 
                fromUser: req.user, 
                text: 'new notification',
                subject: activeUser.id,
                subjectType: 'geeb w waddy accept your request to be sales Man'
            });
            let notif = {
                "description":'geeb w waddy accept your request to be sales Man',
                "arabicDescription":"تم الموافقه على طلب انضمامك الى مندوبينا  "
            }
            await Notif.create({...notif,resource:req.user,target:userId,user:activeUser.id});
            res.send('user active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = false;
            await activeUser.save();
            let reports = {
                "action":"Dis-Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user disactive');
        } catch (error) {
            next(error);
        }
    },
    async block(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = true;
            await activeUser.save();
            let reports = {
                "action":"block User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user block');
        } catch (error) {
            next(error);
        }
    },
    async unblock(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = false;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user active');
        } catch (error) {
            next(error);
        }
    },

    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id);
            res.send(user);
        } catch (error) {
            next(error);
        }
    },

    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Email Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },

    async checkExistPhone(req, res, next) {
        try {
            let phone = req.body.phone;
            if (!phone) {
                return next(new ApiError(400, 'phone is required'));
            }
            let exist = await User.findOne({ phone: phone });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Mobile Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },
    validateUpdatedBody(isUpdate = true) {
        let validations = [
            body('username').not().isEmpty().withMessage('username is required'),
            body('gender').not().isEmpty().withMessage('gender is required'),
            body('birthYear').not().isEmpty().withMessage('birthYear is required'),
            body('language'),
            body('rate'),
            body('balance'),
            body('debt'),
            body('ratePercent'),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    userQuery._id = { $ne: req.user._id };
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },
    async updateInfo(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(req.user._id, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            user.language = validatedBody.language;
            user.phone = validatedBody.phone;
            user.gender = validatedBody.gender;
            user.birthYear = validatedBody.birthYear;

            user.username = validatedBody.username;
            user.email = validatedBody.email;
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
           
            await user.save();
            let reports = {
                "action":"Update User Info",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    }, 
    validateUpdatedBodyAdmin(isUpdate = true) {
        let validations = [
            body('username').not().isEmpty().withMessage('username is required'),
            body('gender').not().isEmpty().withMessage('gender is required'),
            body('birthYear').not().isEmpty().withMessage('birthYear is required'),
            body('language'),
            body('rate'),
            body('balance'),
            body('debt'),
            body('transportType'),
            body('manufacturingYear'),
            body('bank'),
            body('bankNumber'),

            body('ratePercent'),
       
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { phone: value };
                    if (isUpdate && user.phone === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
            }),
            body('email')
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            }),
    
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },
    async updateInfoAdmin(req, res, next) {
        try {
            let {userId} = req.params;
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            user.language = validatedBody.language;
            user.phone = validatedBody.phone;
            user.gender = validatedBody.gender;
            user.birthYear = validatedBody.birthYear;

            user.username = validatedBody.username;
            user.email = validatedBody.email;
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
            if(validatedBody.balance){
                user.balance = validatedBody.balance;
            }
            if(validatedBody.debt){
                user.debt = validatedBody.debt;
            }
            if(validatedBody.rate){
                user.rate = validatedBody.rate;
            }
            if(validatedBody.ratePercent){
                user.ratePercent = validatedBody.ratePercent;
            }
            if(validatedBody.transportType){
                user.transportType = validatedBody.transportType;
            }
            if(validatedBody.manufacturingYear){
                user.manufacturingYear = validatedBody.manufacturingYear;
            }
            if(validatedBody.bank){
                user.bank = validatedBody.bank;
            }
            if(validatedBody.bankNumber){
                user.bankNumber = validatedBody.bankNumber;
            }
            await user.save();
            let reports = {
                "action":"Update User Info",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
    validateSendCode() {
        return [
            body('email').not().isEmpty().withMessage('email Required')
        ];
    },
    async sendCodeToEmail(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            user.verifycode = generateVerifyCode();
            await user.save();
            //send code
            let text = user.verifycode.toString()
            sendEmail(validatedBody.email, text)
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
            body('email').not().isEmpty().withMessage('email Required'),
        ];
    },
    async resetPhoneConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPhone() {
        return [
            body('email').not().isEmpty().withMessage('email is required'),
            body('newPhone').not().isEmpty().withMessage('new phone is required')
        ];
    },

    async resetPhone(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);

            user.phone = validatedBody.newPhone;
            user.verifyCode = '0000';
            await user.save();
            let reports = {
                "action":"User reset Phone",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },


    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async addCoupon(req,res,next){
        try{
            let user = await checkExistThenGet(req.user._id, User);
            let coupon = await Coupon.find({deleted:false,end:false,couponNumber:req.body.couponNumber}).select("_id");
            if(user.country != coupon.country)
                  return next(new ApiError(403, ('this coupon outside your country')));
            user.hasCoupon = true;
            user.coupon = coupon[0]._id;
            await user.save();

            res.status(200).send(user);
        } catch(err){
            next(new ApiError(403, ('invalid coupon')))
        }
    },
    async reduceBalance(req,res,next){
        try{
            let {userId} = req.params;
            let client = await checkExistThenGet(userId, User);
            if(req.body.money > client.balance)
            return next(new ApiError(403, ('this client did not have this balance')));
            let newBalance = client.balance - req.body.money
            client.balance = newBalance
                await client.save();
            let user = await checkExistThenGet(req.user._id, User);
            let newSBalance = user.balance + req.body.money
                user.balance = newSBalance
                await user.save();
            res.status(200).send("sucess");
        } catch(err){
            next(err)
        }
    },
    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { type,active,country } = req.query;
            let query = {deleted: false };

            if (type)
                query.type = type;
            if (active){
                query.active = active;
            } 
            if (country){
                query.country = country;
            }
            let users = await User.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);

            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let { userId } = req.params;
            let user = await checkExistThenGet(userId, User);
            user.deleted = true;
            await user.save();
            //await User.findByIdAndDelete(userId);
            let reports = {
                "action":"Delete user",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async getUser(req, res, next) {
        try {
            res.send(
                await checkExistThenGet(req.user._id, User)
            );
        } catch (error) {
            next(error)
        }
    },
    async socialLogin(req, res, next) {
        try{
            let user = await User.findOne({email:req.body.email});
            console.log(user)
            
            if(!user){
                if(req.body.country){
                    let country = await checkExistThenGet(req.body.country, Country);
                    if(country.enableGift == true){
                        user = await User.create({
                            email: req.body.email,
                            type:'CLIENT',
                            phone:' ',
                            gender:' ',
                            username:' ',
                            birthYear:' ',
                            Giftbalance:country.giftBalance
                        });
                    }
                } else{
                    user = await User.create({
                        email: req.body.email,
                        type:'CLIENT',
                        phone:' ',
                        gender:' ',
                        username:' ',
                        birthYear:' ',
                    });
                }
            }

            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            res.status(200).send({
                user,
                token: generateToken(user.id)
            });
            
        } catch(err){
            next(err);
        }
    },


};
